%% Inverse Kinematics Demo for a single leg of the hybrid robot,which is currently under developement at Control System Laboratory at NTUA


% This demonstration performs inverse kinematics of a
% robotic leg following a desired trajectory described by a set of waypoints.

%% Load and display robot
clear
clc
addpath(genpath('/home/janfelix/catkin_ws/src/hybrid_robot'));      %add folder and subfolders to the search path

%make sure to change your host name and the path to where your "hybrid robot" directory is located

robot = importrobot('right_leg.xacro');       %here we import our xacro file using the robotic toolbox function importrobot

%now we just display our robot
axes = show(robot);
%set axes properties to get a satisfactory view on the leg
figure(1);
axes.XLim = [-0.3 0.3];
axes.YLim = [-0.3 0.3];
axes.ZLim = [0 0.5];
axes.CameraPositionMode = 'auto';
camorbit(-90,0);

%% Create a set of desired wayPoints for trajectory

% wayPoints = [0.2 -0.2 0.02;0.25 0 0.15; 0.2 0.2 0.02]; 
wayPoints = [0.05 -0.02 0.26; 0.1 -0.02 0.32; 0.07 -0.02 0.35];
exampleHelperPlotWaypoints(wayPoints);

%% Create a smooth curve from the waypoints to serve as trajectory

trajectory=cscvn(wayPoints');

% Plot trajectory spline and waypoints
hold on
fnplt(trajectory,'r',2);

%% Perform Inverse Kinematics for a point in space

% now we perform the Inverse Kinematics for the given set of waypoints


%Initializing the GeneralizedInversKinematics object
gik = robotics.GeneralizedInverseKinematics('RigidBodyTree',robot); 

%Set Constraints for the GeneralizedInverseKinematics object
gik.ConstraintInputs = {'position',...  % Position constraint for closed-loop mechanism
                        'position',...  % Position constraint for end-effector
                        };       % possible to include joint limits with 'joints' 
gik.SolverParameters.AllowRandomRestart = false;
initialguess = robot.homeConfiguration; %Extracts joint state of homeConfiguration as definded in your xacro file


%Position 1 constraints
%define position targets, positionTarget1 is supposed to close the loop
%between connector_1 (as "endeffector") and the lower_leg (as
%"ReferenceBody")
positionTarget1 = robotics.PositionTarget('connector_1','ReferenceBody','lower_leg');
positionTarget1.TargetPosition = [-0.025 0 0];
positionTarget1.Weights = 50;
positionTarget1.PositionTolerance = 1e-6;

%position 2 constraints
%position constraint between wheel as "endeffector" to always match a
%desiredEndeffector Position
desiredEEPosition = [0 0 0]; 
positionTarget2 = robotics.PositionTarget('wheel');
positionTarget2.TargetPosition = desiredEEPosition;
positionTarget2.PositionTolerance = 1e-6;
positionTarget2.Weights = 1;

% Calculate the inverse kinematic solution using the "gik" solver 
%set a number of Points in which the trajectory is descretise into.
numTotalPoints = 30;

% Evaluate trajectory to create a vector of end-effector positions
eePositions = ppval(trajectory,linspace(0,trajectory.breaks(end),numTotalPoints));

% Call inverse kinematics solver for every end-effector position using the
% previous configuration as initial guess
for idx = 1:size(eePositions,2)
    desiredEEPosition = eePositions(:,idx)';
    positionTarget2.TargetPosition = desiredEEPosition;
    configSoln(idx,:) = gik(initialguess,positionTarget1,positionTarget2);
    initialguess = configSoln(idx,:);
end

%% Visualize robot configurations
title('Robot waypoint tracking visualization')
%axis([-0.1 0.4 -0.35 0.35 0 0.35]);
for idx = 1:size(eePositions,2)
    show(robot,configSoln(idx,:), 'PreservePlot', false,'Frames','off');
    pause(0.1)
end
hold off


%% Visualize the joint states of the acutated joints to follow the trajectory
figure(2);
Time_Moving = 20;

Discrete_Time_Point = Time_Moving/numTotalPoints;
joint_A_trajectory = zeros(2,numTotalPoints);
joint_prismatic_B2_trajectory = zeros(2,numTotalPoints);
joint_E_trajectory = zeros(2,numTotalPoints);
time = Discrete_Time_Point;

for i = 1 : numTotalPoints

    joint_A_trajectory(1,i) = time;
    joint_prismatic_B2_trajectory(1,i) = time;
    joint_E_trajectory(1,i) = time;
    joint_A_trajectory(2,i) = configSoln(i,1).JointPosition;
    joint_prismatic_B2_trajectory(2,i) = configSoln(i,3).JointPosition;
    joint_E_trajectory(2,i) = configSoln(i,5).JointPosition;
    time = time + Discrete_Time_Point;   
end

%trajectory_Joint_A=cscvn(Joint_A_trajectory);
%fnplt(trajectory_Joint_A,'r',2);

%% Example in MAtlab



plot(joint_A_trajectory(1,:),joint_A_trajectory(2,:),'ro','LineWidth',2);
hold on
plot(joint_prismatic_B2_trajectory(1,:),joint_prismatic_B2_trajectory(2,:),'ro','LineWidth',2);

%ax = gca;
%ax.XTick = [];
%ax.YTick = [];


fnplt(cscvn(joint_A_trajectory),'r',2)
fnplt(cscvn(joint_prismatic_B2_trajectory),'r',2)
fnplt(cscvn(joint_E_trajectory),'r',2)
hold off