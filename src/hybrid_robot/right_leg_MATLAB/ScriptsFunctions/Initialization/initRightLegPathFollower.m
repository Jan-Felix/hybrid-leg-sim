%% Initialization Script -- Right Leg Path Follower

% This script is used in the model callbacks to initialize 
% all necessary data for the corresponding simulation.

%% Load robot into the workspace

%addpath(genpath('/home/janfelix/catkin_ws/src/hybrid_robot'));
%robot = importrobot('right_leg.xacro');


%% Define waypoints based on case provided
%switch trajectorySelection
    %case 1 % Simple Test Trajectory
        wayPoints = [0.05 -0.02 0.26; 0.1 -0.02 0.32; 0.07 -0.02 0.35];
    %case 2 % Complex Trajectory
        %wayPoints = [0.2 -0.2 0.02; 0.15 0 0.28; 0.15 0.05 0.2; ...
     %                0.15 0.09 0.15; 0.1 0.12 0.1; 0.04 0.1 0.2; ...
      %               0.25 0 0.15; 0.2 0.2 0.02];r
    %case 3 % Trajectory that will cause gripper fault
        %wayPoints = [0.2 -0.2 0.02; 0.15 0 0.28; 0.15 0.05 0.2; ...
        %             0.15 0.09 0.15; 0.1 0.12 0.1; 0.04 0.1 0.2; ...
        %             0.25 0 0.15; 0.2 0.2 0.02];
%clear end
% Use Curve Fitting Toolbox to create a spline from the trajectory points
trajectory = cscvn(wayPoints');
numTotalPoints = 20;
eePositions = ppval(trajectory,linspace(0,trajectory.breaks(end),numTotalPoints));
trajectoryPoints = eePositions';


%% Robot arm control parameters
Ts = 0.05;  % Sample time
%filter_constant = 0.05; % Time constant for joint actuators
%gripper_force = 3; % Gripper force
position_threshold = 0.005;
%joint_damping = 0.001;

%% Inverse Kinematics parameters



