function [jointStates, iterations] = simInvKin(targetPosition,previousConfig)
% This function is meant to be implemented as interpreted in MATLAB
% function blocks to provide inverse kinematics algorithms for simulations


%initialGuess = struct('jointName',{field1,field2,field3,field4,field5},'jointPosition',{value1,value2,value3,value4,value5});
%targetPosition = [0;0;0];
%previousConfig = [0;0;0;0;0];

    persistent simRobot;
    persistent previousStates;
    persistent gik;
    persistent positionTarget1;
    persistent positionTarget2;

    % Following statement runs only once at initialization to create the rigid
    % body tree object and inital configuration. The persistent variables store data throughout the
    % course of the simulation. This makes the object reusable and ensures an
    % efficient simulation

    %calling script "createRigidBodyTree.m" to load robot description and
    %use home.configuration initialization value for previousStates
    if(isempty(simRobot))
       [simRobot,previousStates] = createRigidBodyTree();
       gik = robotics.GeneralizedInverseKinematics('RigidBodyTree',simRobot);
       
       gik.ConstraintInputs = {'position',...  % Position constraint for closed-loop mechanism
                        'position',...  % Position constraint for end-effector
                        };       % possible to include joint limits with 'joints' 
       gik.SolverParameters.AllowRandomRestart = false;
   
       
       
       %Position 1 constraints
       positionTarget1 = robotics.PositionTarget('connector_1','ReferenceBody','lower_leg');
       positionTarget1.TargetPosition = [-0.025 0 0];
       positionTarget1.Weights = 50;
       positionTarget1.PositionTolerance = 1e-6;
       %position 2 constraints
       desiredEEPosition = [0 0 0]; 
       positionTarget2 = robotics.PositionTarget('wheel');
       positionTarget2.TargetPosition = desiredEEPosition;
       positionTarget2.PositionTolerance = 1e-6;
       positionTarget2.Weights = 1;
   end



    % Populate the previous configuration structure with the input data from
    % simulation, use current JointState Data to get current configuration
    % of the robot
     for idx = 1:5
         previousStates(idx).JointPosition = previousConfig(idx);
     end

    % Calculate the inverse kinematic solution using the "gik" solver 
    positionTarget2.TargetPosition = (targetPosition)';
    [configSoln, info] = gik(previousStates,positionTarget1, positionTarget2);
    iterations=info.Iterations;

    % Assign to the output variable from the robot configuration structure
    for idx = 1:5
        jointStates(idx) = configSoln(idx).JointPosition;
    end

end

